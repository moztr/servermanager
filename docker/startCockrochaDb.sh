#!/bin/bash

docker rm -f sdtmcockroach
docker run --name sdtmcockroach -h sdtmpostgres \
  -v "/home/vagrant/mine/sdtd-cockroach:/cockroach/cockroach-data" \
  -p 5432:5432 -p 26257:26257 -p 8080:8080 \
  cockroachdb/cockroach:v1.1.2 start --insecure

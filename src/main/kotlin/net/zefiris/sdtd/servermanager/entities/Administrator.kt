package net.zefiris.sdtd.servermanager.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class Administrator(
        @Id var uuid: UUID = UUID.randomUUID(),

        var username: String? = null,

        var email: String? = null,

        var enabled: Boolean = false,

        var administrator: Boolean = false
)

package net.zefiris.sdtd.servermanager.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class SteamUser(
        @Id val uuid : UUID= UUID.randomUUID(),
        var steam64Id : String? = null
)
package net.zefiris.sdtd.servermanager.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.*
import javax.validation.constraints.Size

@Document
data class Server(
        @Id var uuid : UUID = UUID.randomUUID(),

        var name: String = "default",

        @DBRef
        var administrators: Set<Administrator>? = HashSet(),

        @DBRef
        var players: Set<Player>? = HashSet(),

        var created: Instant? = null,

        var updated: Instant? = null,

        var description: String? = null,

        var address: String? = null,

        var port: Int? = null,

        var sshPort: Int = 22,

        var sshPrivateKey : String? = null,

        var sshPublicKey : String? = null,

        var sshUser : String = "sdtd",

        var sshHost : String = "localhost",

        var useSshControl : Boolean = false,

        var allocControlScript : String = "sudo /usr/local/bin/7dtd.sh",

        var password: String? = null,

        var active: Boolean? = null,

        var whitelist: String? = null,

        var minimumPing: Int? = null,

        var telnetEnabled: Boolean = false,
        
        var telnetPort: Int = 0,

        var telnetPassword: String? = null,

        var daemonEnabled: Boolean = false,

        var daemonPathToExec: String = "",

        var daemonPathToConfig : String = "",

        var daemonWorkdir: String? = null,

        var rungarbagecollection : Boolean = false,

        var forcedRestart : Boolean = false,

        var forcedRestartAfterXMinutes : Int = 0,

        var gracefulRestartAfterXMinutes : Int = 600,

        var gracefulRestartLowPlayerCount : Int = 0,

        @Size(max = 20000) var daemonServerconfig: String? = null,

        @Size(max = 1000) var welcomeMessage: String? = null,

        var lastTelnetHeartbeat: Instant? = null

    /*var teleportCooldownInSeconds: Int? = null,

    var teleportDelayInSeconds: Int? = null,

    var teleportMaximumHomeDistance: Int? = null,

    var teleportMinimumPlayerDistance: Int? = null,

    var teleportEnabled: Boolean = false*/
)

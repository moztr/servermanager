package net.zefiris.sdtd.servermanager.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant
import java.util.*

@Document
data class Player(
        @Id val uuid: UUID = UUID.randomUUID(),
        @DBRef val steamUser: SteamUser,
        var created: Instant = Instant.now(),
        var permissionLevel: Int? = null,
        var banned: Boolean = false,
        var bannedReason: String? = null,
        var bannedUntil: Instant? = null
) {
    private constructor() : this(steamUser = SteamUser())
}
package net.zefiris.sdtd.servermanager.server.ssh

import net.zefiris.sdtd.servermanager.entities.Server
import org.springframework.stereotype.Component

/**
 * Connect via ssh to remote server and use alloc scripts to get info and control the server
 */
@Component
class AllocScriptsViaSSHGateway(private val sshGateway : SshGateway) {

    fun kill(server: Server) {
        sshGateway.executeCommand(server, "docker stop ${server.name}")
    }

    fun start(server: Server) {
        sshGateway.executeCommand(server, "docker start ${server.name}")
    }

    fun restart(server: Server) {
        sshGateway.executeCommand(server, "docker restart ${server.name}")
    }


}

package net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus

import java.time.Instant

class ServerStatusParser {

    private val r = Regex(".*Time: (.*)\\.[0-9][0-9]m FPS: (.*) Heap: (.*)\\.[0-9]+MB Max: (.*)\\.[0-9]+MB Chunks: (.*) CGO: (.*) Ply: (.*) Zom: (.*) Ent: (.*) Items: (.*) CO: (.*) RSS: (.*)\\.[0-9]+MB.*")

    fun couldMatch(line : String) = r.matches(line)

    fun parse(line: String): ServerStatusDTO {
        val statusMatcher = r.find(line)
        if (statusMatcher != null && statusMatcher.groups.size > 1) {

            val time = statusMatcher.groups[1]!!.value.toInt()
            val fps = statusMatcher.groups[2]!!.value.toDouble()
            val heap = statusMatcher.groups[3]!!.value.toInt()
            val max = statusMatcher.groups[4]!!.value.toInt()
            val chunks = statusMatcher.groups[5]!!.value.toInt()
            val cgo = statusMatcher.groups[6]!!.value.toInt()
            val player = statusMatcher.groups[7]!!.value.toInt()
            val zombie = statusMatcher.groups[8]!!.value.toInt()
            val entities = statusMatcher.groups[9]!!.value
            val items = statusMatcher.groups[10]!!.value.toInt()
            val co = statusMatcher.groups[11]!!.value.toInt()
            val rss = statusMatcher.groups[12]!!.value.toInt()

            return ServerStatusDTO(time = time, fps = fps,
                    heap = heap, max = max, chunks = chunks,
                    cgo = cgo, player = player, zombie = zombie, entities = entities,
                    items = items, co = co, rss = rss
            )

        } else {
            throw FatalParseError("Not matched")
        }
    }
}

class FatalParseError(msg : String) : Exception(msg)

data class ServerStatusDTO(
   val timestamp : Instant = Instant.now(), val time : Int, val fps : Double, val heap : Int, val max : Int,
   val chunks : Int, val cgo : Int, val player : Int, val zombie : Int,
   val entities : String, val items : Int, val co: Int, val rss : Int)
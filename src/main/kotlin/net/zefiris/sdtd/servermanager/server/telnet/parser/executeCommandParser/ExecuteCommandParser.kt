package net.zefiris.sdtd.servermanager.server.telnet.parser.executeCommandParser

import java.time.Instant

class ExecuteCommandParser {
    //2018-01-01T15:30:09 4713.706 INF Executing command 'bc-lp' by Telnet from 37.201.5.61:56254
    private val r = Regex("^.*Executing command '([^']+)' by Telnet from ([.0-9]+):([0-9]+)\$")

    fun couldMatch(line : String) = r.matches(line)

    fun parse(line: String): ExecuteCommandDTO {
        val matcher = r.find(line)
        if (matcher != null && matcher.groups.size > 1) {
            return ExecuteCommandDTO(
                    command = matcher.groups[1]!!.value,
                    ip = matcher.groups[2]!!.value,
                    port = matcher.groups[3]!!.value.toInt()
            )
        } else {
            throw FatalParseError("Not matched")
        }
    }
}

class FatalParseError(msg : String) : Exception(msg)

data class ExecuteCommandDTO(
   val timestamp : Instant = Instant.now(),
   val command : String,
   val ip: String,
   val port: Int
)
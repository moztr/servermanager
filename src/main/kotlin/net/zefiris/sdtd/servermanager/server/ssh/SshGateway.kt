package net.zefiris.sdtd.servermanager.server.ssh

import com.jcabi.ssh.Shell
import com.jcabi.ssh.Ssh
import net.zefiris.sdtd.servermanager.entities.Server

import org.springframework.stereotype.Component

@Component
class SshGateway {
    fun executeCommand(server: Server, cmd: String): String {
        val shell = Ssh(server.sshHost, server.sshPort, server.sshUser, server.sshPrivateKey)
        val stdout = Shell.Plain(shell).exec(cmd)
        return stdout
    }
}

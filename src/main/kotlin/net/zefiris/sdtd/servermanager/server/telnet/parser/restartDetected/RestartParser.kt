package net.zefiris.sdtd.servermanager.server.telnet.parser.restartDetected

import java.time.Instant

class RestartParser {

    //2017-12-29T08:56:23 81146.040 INF Executing command 'shutdown' by Telnet from 37.201.5.61:56398
    private val r = Regex(".*INF Executing command 'shutdown'.*")

    fun couldMatch(line : String) = r.matches(line)

    fun parse(line: String): ShutdownEventDTO {
        if (r.matches(line)) {
            return ShutdownEventDTO()
        } else {
            throw FatalParseError("Not matched")
        }
    }
}

class FatalParseError(msg : String) : Exception(msg)

data class ShutdownEventDTO(
   val timestamp : Instant = Instant.now()
)
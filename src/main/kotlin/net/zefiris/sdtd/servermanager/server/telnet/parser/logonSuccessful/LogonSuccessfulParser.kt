package net.zefiris.sdtd.servermanager.server.telnet.parser.logonSuccessful

class LogonSuccessfulParser {
    private val r = Regex(".*Logon successful.*")

    fun couldMatch(line : String) = r.matches(line)
}
package net.zefiris.sdtd.servermanager.server.telnet.parser.serverRestarting

import java.time.Instant


class ServerRestartingParser {
//2017-12-30T22:41:37 24762.175 INF Chat: 'Server': Server Restarting In 3 Minutes.
    private val r = Regex(".*INF Chat: 'Server': Server Restarting In ([0-9]+) Minutes..*")

    fun couldMatch(line: String) = r.matches(line)

    fun parse(line: String): ServerRestartDTO {
        val matcher = r.find(line)

        if (matcher != null && matcher.groups.size > 1) {
            return ServerRestartDTO(restartInMinutes = matcher.groups[1]!!.value.toInt())
        }
        throw FatalParseError("Line not matched $line")
    }
}

class FatalParseError(msg: String) : Exception(msg)

data class ServerRestartDTO(
        val timestamp: Instant = Instant.now(), val restartInMinutes: Int
)
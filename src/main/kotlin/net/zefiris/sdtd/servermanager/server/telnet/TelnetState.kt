package net.zefiris.sdtd.servermanager.server.telnet

enum class TelnetState {
    DISCONNECTED, CONNECTED, AUTHENTICATED, TelnetState, ABORTED, NO_THREAD
}
package net.zefiris.sdtd.servermanager.server.telnet

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import net.zefiris.sdtd.servermanager.ServerRepository
import net.zefiris.sdtd.servermanager.entities.Server
import net.zefiris.sdtd.servermanager.influx.ServerStatusInfluxDBWriter
import net.zefiris.sdtd.servermanager.server.telnet.parser.bc.BCData
import net.zefiris.sdtd.servermanager.server.telnet.parser.bc.BCPlayer
import net.zefiris.sdtd.servermanager.server.telnet.parser.executeCommandParser.ExecuteCommandParser
import net.zefiris.sdtd.servermanager.server.telnet.parser.logonSuccessful.LogonSuccessfulParser
import net.zefiris.sdtd.servermanager.server.telnet.parser.restartDetected.RestartParser
import net.zefiris.sdtd.servermanager.server.telnet.parser.serverRestarting.ServerRestartingParser
import net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus.ServerStatusDTO
import net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus.ServerStatusParser
import org.apache.commons.net.telnet.EchoOptionHandler
import org.apache.commons.net.telnet.SuppressGAOptionHandler
import org.apache.commons.net.telnet.TelnetClient
import org.apache.commons.net.telnet.TerminalTypeOptionHandler
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintWriter
import java.time.Instant
import java.util.*
import javax.annotation.PostConstruct

@Component
class TelnetRunnerService(
        val serverRepository: ServerRepository,
        val influxDbWriter : ServerStatusInfluxDBWriter
) {

    private val log = LoggerFactory.getLogger(TelnetRunnerService::class.java)

    private val runner = HashMap<UUID, TelnetRunnerWrapper>()

    data class RunnerRTO(
            val uuid : String,
            val lastActivity : Instant,
            val isAlive : Boolean
    )

    fun telnetRunnersRTO () : List<RunnerRTO> {
        synchronized(runner) {
            return runner.map { RunnerRTO( it.key.toString(), it.value.runner.lastActivity, it.value.thread.isAlive) }
        }
    }

    @PostConstruct
    fun initializeRunners() {

    }

    @Scheduled(fixedDelay = 60000, initialDelay = 10000)
    @Synchronized
    fun checkTelnetRunners() {
        // Ensures telnet runners are connected and dead ones are collected
        log.info("RUNNER  checkTelnetRunners go")
        synchronized(runner) {
            serverRepository.findAll().forEach {
                if (!runner.containsKey(it.uuid)) {
                    val telnetRunner = TelnetRunner(this, it.uuid)
                    val thread = Thread(telnetRunner)
                    runner.put(it.uuid, TelnetRunnerWrapper(thread, telnetRunner))
                }
            }

            runner.values.forEach {
                if (!it.thread.isAlive) {
                    log.info("RUNNER ${it.runner.uuid} not alive, starting!")
                    try {
                        it.thread.start()
                    } catch (ex: Exception) {
                        log.warn("issue starting thread", ex)
                        it.thread = Thread(it.runner)
                    }
                } else {

                    if (it.runner.lastActivity.isBefore(Instant.now().minusSeconds(120))) {
                        it.runner.askForStats()
                    }

                    if (it.runner.lastActivity.isBefore(Instant.now().minusSeconds(150))) {
                        log.info("RUNNER ${it.runner.uuid} Last activity of server is too long ago")

                        try {
                            it.thread.join(1000)
                            it.thread.stop()
                        } catch (ex: Exception) {
                            log.warn("Noes ", ex)
                        }

                        log.info("RUNNER ${it.runner.uuid} Restarting thread")
                        try {
                            it.thread = Thread(it.runner)
                            it.thread.start()
                        } catch (ex: Exception) {
                            log.warn("Oops ", ex)
                        }
                    }
                }
            }
        }
        log.info("RUNNER checkTelnetRunner finished")
    }

    // TODO: clean this up, it's a mess!
    fun shutdown(uuid: String) {
        val runner = runner.get(UUID.fromString(uuid))
        if (runner == null) {
            log.warn("server not found")
            return
        }
        runner.runner.shutdown()
    }

    // TODO: clean this up, it's a mess!
    fun shutdownWithCountdown(uuid: String) {
        val runner = runner.get(UUID.fromString(uuid))
        if (runner == null) {
            log.warn("server not found")
            return
        }
        runner.runner.shutdownWithCountdown()
    }

    // TODO: clean this up, it's a mess!
    fun saveWorld(uuid: String) {
        val runner = runner.get(UUID.fromString(uuid))
        if (runner == null) {
            log.warn("server not found")
            return
        }
        runner.runner.saveWorld()
    }


    fun serverStatus(server: Server): ServerStatusDTO? {
        synchronized(runner) {
            val wrapper = runner.get(server.uuid) ?: return null
            return wrapper.runner.lastServerStatus
        }
    }

    fun telnetStatus(server: Server) : TelnetState {
        synchronized(runner) {
            val wrapper = runner.get(server.uuid) ?: return TelnetState.NO_THREAD
            return wrapper.runner.state
        }
    }
}

data class TelnetRunnerWrapper(var thread: Thread, val runner: TelnetRunner)

class TelnetRunner(val service: TelnetRunnerService, val uuid: UUID) : Runnable {

    private val log = LoggerFactory.getLogger(TelnetRunner::class.java)

    var lastActivity: Instant = Instant.now()

    var shouldAbort = false

    var errorCounter = 0

    var lastError: Date? = null

    var lastServerStatus: ServerStatusDTO? = null

    var server: Server? = null

    var writer: PrintWriter? = null

    var reader: BufferedReader? = null

    var state = TelnetState.DISCONNECTED

    var lastrungarbagecollection : Instant = Instant.now()

    var gracefulRestartInProgress : Boolean = false

    var forceRestartInProgress : Boolean = false

    var lastUpdateServerFromDatabase = Instant.now()

    val restartParser = RestartParser()

    var localTelnetPort : Int = 0

    var telnetConnectionUuid = "UNINITIALIZED"

    override fun run() {
        telnetConnectionUuid = UUID.randomUUID().toString()

        // Reinitialize variables
        forceRestartInProgress = false
        gracefulRestartInProgress = false
        state = TelnetState.DISCONNECTED

        lastActivity = Instant.now()
        this.server = service.serverRepository.findById(uuid).get()
        log.info("server {}", server)

        while (!shouldAbort && server!!.telnetEnabled) {
            // load server before connecting

            val telnetClient = TelnetClient()

            try {
                // Re-connect log

                log.info("{} Connecting to {}:{}", server!!.uuid, server!!.address, server!!.telnetPort)

                telnetClient.addOptionHandler(TerminalTypeOptionHandler("VT100", false, false, false, false))
                telnetClient.addOptionHandler(EchoOptionHandler(false, false, false, false))
                telnetClient.addOptionHandler(SuppressGAOptionHandler(true, true, true, true))
                telnetClient.connect(server!!.address, server!!.telnetPort)
                state = TelnetState.CONNECTED

                val writer = PrintWriter(telnetClient.outputStream)
                this.writer = writer
                val reader = BufferedReader(InputStreamReader(telnetClient.inputStream))
                this.reader = reader

                var line: String
                while (!shouldAbort && telnetClient.isConnected) {
                    line = reader.readLine()
                    if (line == null) {
                        break
                    }

                    lastActivity = Instant.now()

                    if (lastUpdateServerFromDatabase.plusSeconds(30).isBefore(Instant.now())) {
                        this.server = service.serverRepository.findById(uuid).get()
                        logLine("refreshed server")
                        lastUpdateServerFromDatabase = Instant.now()
                    }

                    if (state == TelnetState.AUTHENTICATED &&
                            server!!.rungarbagecollection &&
                            lastrungarbagecollection.plusSeconds(120).isBefore(Instant.now())
                            ) {
                        logLine("run garbage collection")
                        askForStats()
                        lastrungarbagecollection = Instant.now()
                    }


                    handleLine(line = line)

                    logLine("Read line: '$line'")
                }

            } catch (e: Exception) {
                log.info("$uuid Exception in telnet thread, exiting!", e)
                state = TelnetState.ABORTED
                return
            } finally {
                telnetClient.disconnect()
            }
        }
    }

    @Synchronized
    fun askForStats() {
        writeLine("mem")
    }

    @Synchronized
    fun writeLine(line: String) {
        try {
            logLine("Write line: '$line'")
            writer!!.println(line)
            writer!!.flush()
        } catch (e: Exception) {
            log.info("Exception while sending msg", e)
        }
    }

    val serverStatusParser = ServerStatusParser()

    val logonSuccessfulParser = LogonSuccessfulParser()

    val executeCommandParser = ExecuteCommandParser()

    val serverRestartingParser = ServerRestartingParser()


    private fun handleLine(line: String) {
        if (line.contains("Infinity or NaN floating point")) {
            errorCounter++
            lastError = Date()
            return
        }

        if (line.contains("GMSG: Server:")) {
            return
        }

        // Check if we are going to get a reponse for a command that we sent earlier
        if (executeCommandParser.couldMatch(line)) {
            val executeCommand = executeCommandParser.parse(line)
            if (localTelnetPort == 0 && executeCommand.command == "version /tag="+telnetConnectionUuid) {
                localTelnetPort = executeCommand.port
                writeLine("bc-lp")
            }
            if (executeCommand.port == localTelnetPort) {
                logLine("Detected executed command: '${executeCommand.command}'")
                if (executeCommand.command == "bc-lp") {
                    // ok, now let's parse until the end ...
                    var completeData = ""
                    var thisLine = ""
                    while (!thisLine.endsWith("]")) {
                        thisLine = reader!!.readLine()
                        completeData += thisLine
                    }

                    val mapper = jacksonObjectMapper()
                    val bcPlayers : List<BCPlayer> = mapper.readValue(completeData)
                    logLine("Ok, found bc-lp data: $completeData")
                }
            }
        }
        // TODO: regex it
        if ("Please enter password:" == line || "\u0000Password incorrect, please enter password:" == line || line.contains("Password incorrect")) run {
            logLine("Sending password")
            writeLine(server!!.telnetPassword!!)
        } else if (logonSuccessfulParser.couldMatch(line)) {
            changeState(TelnetState.AUTHENTICATED)
            writeLine("version /tag=$telnetConnectionUuid")
        } else if (serverStatusParser.couldMatch(line)) {
            val serverStatus = serverStatusParser.parse(line)
            lastServerStatus = serverStatus
            logLine("Found server status $serverStatus")
            service.influxDbWriter.write(serverStatus, server!!.uuid.toString())

            if (!gracefulRestartInProgress &&
                    serverStatus.time > server!!.gracefulRestartAfterXMinutes &&
                    serverStatus.player <= server!!.gracefulRestartLowPlayerCount) {
                gracefulRestartInProgress = true
                logLine("Graceful restart condition met, restarting")
                this.writeLine("say \"Automatic graceful server restart\"")
                this.writeLine("shutdown")
            }

            logLine("forcedRestart debug: $forceRestartInProgress ${server!!.forcedRestart} ${serverStatus.time} ${server!!.forcedRestartAfterXMinutes}")
            if (!forceRestartInProgress &&
                    server!!.forcedRestart &&
                    serverStatus.time > server!!.forcedRestartAfterXMinutes ) {
                forceRestartInProgress = true
                logLine("Forced restart condition met, restarting")
                this.writeLine("say \"Automatic server restart\"")
                this.writeLine("stopserver 5")
            }
        } else if (restartParser.couldMatch(line) ) {
            val restartParsed = restartParser.parse(line)
            service.influxDbWriter.writeRestartDetected(server!!.uuid.toString())
            logLine("Restart detected, written to influxdb")

        } else if (serverRestartingParser.couldMatch(line)) {
            val restartingParsed = serverRestartingParser.parse(line)
            logLine("Restart in: " + restartingParsed.restartInMinutes)
        } else {
            logLine("unknown: '$line'")
        }
    }

    fun logLine(line: String) {
        log.info(this.state.toString() + " $uuid ${server!!.name} $line")
    }

    fun changeState(telnetState: TelnetState) {
        this.state = telnetState
    }

    fun shutdown() {
        this.writeLine("shutdown")
    }

    fun shutdownWithCountdown() {
        this.writeLine("stopserver 5")
    }

    fun saveWorld() {
        this.writeLine("sa")
    }

}


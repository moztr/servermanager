package net.zefiris.sdtd.servermanager

import net.zefiris.sdtd.servermanager.entities.Administrator
import net.zefiris.sdtd.servermanager.entities.Player
import net.zefiris.sdtd.servermanager.entities.Server
import net.zefiris.sdtd.servermanager.entities.SteamUser
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.PropertySource
import org.springframework.context.support.beans
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.scheduling.annotation.EnableScheduling
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@PropertySource("classpath:application.properties")
class ServermanagerApplication

fun main(args: Array<String>) {
    SpringApplicationBuilder()
            .sources(ServermanagerApplication::class.java)
            .initializers(beans {
                bean {
                    ApplicationRunner {

                        val serverRepository = ref<ServerRepository>()
                        serverRepository.findAll().forEach {
                            println(it)
                        }

                        val userRepository = ref<UserRepository>()
                        userRepository.findAll().forEach {
                            println(it)
                        }
/*                        val server = Server(name = "3-pvp-starvation")
                        serverRepository.save(server)
                        val administrator = Administrator(username = "HappyHippo " + Instant.now().epochSecond)
                        userRepository.save(administrator)
                        
                        val steamUserRepository = ref<SteamUserRepository>()
                        val steamUser = steamUserRepository.save(SteamUser(steam64Id = "STEAM64ID2"))

                        val playerRepository = ref<PlayerRepository>()
                        playerRepository.save(Player(steamUser = steamUser))*/
                    }
                }
            })
            .run(*args)
}


interface ServerRepository : MongoRepository<Server, UUID>

interface UserRepository : MongoRepository<Administrator, UUID>

interface PlayerRepository : MongoRepository<Player, UUID>

interface SteamUserRepository : MongoRepository<SteamUser, UUID>
package net.zefiris.sdtd.servermanager.api.rto

import net.zefiris.sdtd.servermanager.server.telnet.TelnetState
import net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus.ServerStatusDTO

data class CompleteServerStatusRTO (
        val serverStatus : ServerStatusDTO?,
        val telnetState : TelnetState
)
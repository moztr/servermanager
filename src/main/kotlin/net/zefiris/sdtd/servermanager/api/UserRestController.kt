package net.zefiris.sdtd.servermanager.api;

import net.zefiris.sdtd.servermanager.UserRepository
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class UserRestController(private val userRepository: UserRepository) {

    @GetMapping("/users")
    fun users() = this.userRepository.findAll()
}

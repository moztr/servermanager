package net.zefiris.sdtd.servermanager.api

import net.zefiris.sdtd.servermanager.server.telnet.TelnetRunnerService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class TelnetRunnerRestController(private val telnetRunnerService : TelnetRunnerService) {

    @GetMapping("/runner")
    fun get()  = telnetRunnerService.telnetRunnersRTO()


}
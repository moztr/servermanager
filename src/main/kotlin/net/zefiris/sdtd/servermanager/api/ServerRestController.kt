package net.zefiris.sdtd.servermanager.api

import net.zefiris.sdtd.servermanager.ServerRepository
import net.zefiris.sdtd.servermanager.api.rto.CompleteServerStatusRTO
import net.zefiris.sdtd.servermanager.api.rto.ServerRTO
import net.zefiris.sdtd.servermanager.entities.Server
import net.zefiris.sdtd.servermanager.server.ssh.AllocScriptsViaSSHGateway
import net.zefiris.sdtd.servermanager.server.telnet.TelnetRunnerService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
class ServerRestController(private val serverRepository: ServerRepository, private val allocScriptsViaSSHGateway : AllocScriptsViaSSHGateway, private val telnetRunnerService : TelnetRunnerService) {

    @GetMapping("/server")
    fun server() = this.serverRepository.findAll().map { ServerRTO(uuid = it.uuid.toString(), name = it.name, daemonPathToExec = it.daemonPathToExec,daemonEnabled = it.daemonEnabled, daemonPathToConfig =  it.daemonPathToConfig) }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Server not found")
    class ServerNotFoundException: RuntimeException()

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Status not found")
    class StatusNotFoundException: RuntimeException()

    @GetMapping("/server/{uuid}/status")
    fun status(@PathVariable("uuid") uuid : String) : CompleteServerStatusRTO
    {
        val server = (serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()).get()
        return CompleteServerStatusRTO(
                telnetRunnerService.serverStatus(server),
                telnetRunnerService.telnetStatus(server)
        )
    }

    @GetMapping("/server/{uuid}")
    fun get(@PathVariable("uuid") uuid : String) : ServerRTO {
        val server = serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()
        val s = server.get()
        return ServerRTO(uuid = s.uuid.toString(), name = s.name, daemonEnabled = s.daemonEnabled, daemonPathToExec = s.daemonPathToExec, daemonPathToConfig = s.daemonPathToConfig)
    }

    @PostMapping("/server/{uuid}/kill")
    fun kill(@PathVariable("uuid") uuid : String) {
        val server = (serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()).get()
        allocScriptsViaSSHGateway.kill(server)
    }

    @PostMapping("/server/{uuid}/restart")
    fun restart(@PathVariable("uuid") uuid : String) {
        val server = (serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()).get()
        allocScriptsViaSSHGateway.restart(server)
    }

    @PostMapping("/server/{uuid}/shutdown")
    fun shutdown(@PathVariable("uuid") uuid : String) {
        telnetRunnerService.shutdown(uuid)
    }

    @PostMapping("/server/{uuid}/stopserver")
    fun shutdownWithCountdown(@PathVariable("uuid") uuid : String) {
        telnetRunnerService.shutdownWithCountdown(uuid)
    }

    @PostMapping("/server/{uuid}/saveWorld")
    fun saveWorld(@PathVariable("uuid") uuid : String) {
        telnetRunnerService.saveWorld(uuid)
    }

    @PostMapping("/server/{uuid}/start")
    fun start(@PathVariable("uuid") uuid : String) {
        val server = (serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()).get()
        allocScriptsViaSSHGateway.start(server)
    }

    @PostMapping("/server")
    fun createNewServer(@RequestBody server : Server) : String {
        serverRepository.save(server)
        return server.uuid.toString()
    }

    @PutMapping("/server/{uuid}")
    fun putServer(@PathVariable("uuid") uuid : String, @RequestBody serverRTO : ServerRTO) : String {
        val server = (serverRepository.findById(UUID.fromString(uuid)) ?: throw ServerNotFoundException()).get()
        server.name = serverRTO.name
        server.daemonEnabled = serverRTO.daemonEnabled
        server.daemonPathToExec = serverRTO.daemonPathToExec
        server.daemonPathToConfig = serverRTO.daemonPathToConfig
        serverRepository.save(server)
        return server.uuid.toString()
    }
}
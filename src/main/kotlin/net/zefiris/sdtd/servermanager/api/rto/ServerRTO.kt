package net.zefiris.sdtd.servermanager.api.rto

data class ServerRTO(
        val uuid : String,
        val name : String,
        var daemonEnabled: Boolean,
        var daemonPathToExec: String,
        var daemonPathToConfig : String
)
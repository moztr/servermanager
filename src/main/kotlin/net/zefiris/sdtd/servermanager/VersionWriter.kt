package net.zefiris.sdtd.servermanager

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class VersionWriter {


    constructor(
            @Value("\${build.date}") buildDate: String
    ) {
        LoggerFactory.getLogger(VersionWriter::class.java).info("BUILDDATE: $buildDate")
    }
}
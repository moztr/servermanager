package net.zefiris.sdtd.servermanager.influx

import net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus.ServerStatusDTO
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class InfluxDbConfiguration(
        @Value("\${servermanager.influxdb.url}") val influxdbUrl: String,
        @Value("\${servermanager.influxdb.username}") val influxdbUsername: String,
        @Value("\${servermanager.influxdb.password}") val influxdbPassword: String,
        @Value("\${servermanager.influxdb.db}") val influxdbDb: String
)


@Component
class ServerStatusInfluxDBWriter(
        val configuration: InfluxDbConfiguration
) {

    private val log = LoggerFactory.getLogger(ServerStatusInfluxDBWriter::class.java)

    fun write(serverStatus: ServerStatusDTO, serverUuid: String) {
        writeToDb { influxDB ->
            influxDB.write(Point.measurement("sdtd")
                    .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                    .tag("uuid", serverUuid)
                    .addField("fps", serverStatus.fps)
                    .addField("player", serverStatus.player)
                    .addField("heap", serverStatus.heap)
                    .addField("max", serverStatus.max)
                    .addField("rss", serverStatus.rss)
                    .addField("zombie", serverStatus.zombie)
                    .addField("cgo", serverStatus.cgo)
                    .addField("uptime", serverStatus.time)
                    .build())
        }
        log.debug("Wrote servertStatus to influxdb")
    }

    fun writeRestartDetected(serverUuid: String) {
        writeToDb { influxDB ->
            influxDB.write(Point.measurement("sdtd")
                    .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                    .tag("uuid", serverUuid)
                    .addField("restartDetected", true)
                    .build())
        }
        log.debug("Wrote restartDetected to influxdb")
    }

    fun writeToDb(f: (InfluxDB) -> Unit) {
        log.debug("Opening InfluxDB connection")
        val influxDB = InfluxDBFactory.connect(configuration.influxdbUrl, configuration.influxdbUsername, configuration.influxdbPassword)
        influxDB.createDatabase(configuration.influxdbDb)
        influxDB.setDatabase(configuration.influxdbDb)
        influxDB.enableBatch(1000, 100, TimeUnit.MILLISECONDS)
        f(influxDB)
        influxDB.flush()
        influxDB.close()
        log.debug("InfluxDB connection closed")
    }


}

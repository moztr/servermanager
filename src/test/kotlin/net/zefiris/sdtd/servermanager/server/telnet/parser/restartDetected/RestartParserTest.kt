package net.zefiris.sdtd.servermanager.server.telnet.parser.restartDetected

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.time.Duration
import java.time.Instant

class RestartParserTest {

    val shouldMatch = "2017-12-29T08:56:23 81146.040 INF Executing command 'shutdown' by Telnet from 37.201.5.61:56398"

    val shouldNotMatch = "2017-12-29T08:56:24 81146.236 INF SaveAndCleanupWorld"


    @Test
    fun couldMatch() {
        assertTrue(RestartParser().couldMatch(shouldMatch))
        assertFalse(RestartParser().couldMatch(shouldNotMatch))
    }

    @Test
    fun parse() {
        val parsed = RestartParser().parse(shouldMatch)
        val difference = Duration.between(parsed.timestamp, Instant.now())
        System.out.println(difference.toMillis())
        assertTrue(difference.toMillis() < 100)
    }

    @Test(expected = FatalParseError::class)
    fun cannotParse() {
        RestartParser().parse(shouldNotMatch)
    }
}
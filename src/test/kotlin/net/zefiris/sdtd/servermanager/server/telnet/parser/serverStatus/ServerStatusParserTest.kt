package net.zefiris.sdtd.servermanager.server.telnet.parser.serverStatus

import org.junit.Test

import org.junit.Assert.*

class ServerStatusParserTest {

    @Test
    fun parseOther() {
        val input = "2017-12-03T19:57:10 3188.706 INF Time: 51.58m FPS: 34.58 Heap: 1982.1MB Max: 2694.9MB Chunks: 2019 CGO: 115 Ply: 4 Zom: 1 Ent: 29 (47) Items: 1 CO: 4 RSS: 5706.6MB"
        assertTrue(ServerStatusParser().couldMatch(input))
    }

    @Test
    fun parseMore() {
        val input = "2017-12-03T20:20:16 4574.580 INF Time: 74.60m FPS: 31.99 Heap: 2322.5MB Max: 2694.9MB Chunks: 2214 CGO: 113 Ply: 5 Zom: 2 Ent: 28 (47) Items: 0 CO: 5 RSS: 6059.6MB"
        assertTrue(ServerStatusParser().couldMatch(input))
    }

    @Test
    fun parse() {
        val input = "2017-12-03T19:25:06 1264.619 INF Time: 19.56m FPS: 36.31 Heap: 1992.9MB Max: 2992.9MB Chunks: 2245 " +
                "CGO: 120 Ply: 7 Zom: 3 Ent: 21 (25) Items: 1 CO: 3 RSS: 5408.2MB"
        val dto = ServerStatusParser().parse(input)
        assertNotNull(dto)

        assertEquals(19, dto.time)
        assertEquals(36.31, dto.fps, 0.01)
        assertEquals(1992, dto.heap)
        assertEquals(2992, dto.max)
        assertEquals(2245, dto.chunks)
        assertEquals(120, dto.cgo)
        assertEquals(7, dto.player)
        assertEquals(1, dto.items)
        assertEquals(3, dto.co)
        assertEquals(5408, dto.rss)

    }
}
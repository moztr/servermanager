package net.zefiris.sdtd.servermanager.server.telnet.parser.serverRestarting

import org.junit.Test

import org.junit.Assert.*

class ServerRestartParserTest {
    val shouldMatch = "2017-12-30T22:41:37 24762.175 INF Chat: 'Server': Server Restarting In 3 Minutes."
    val shouldNotMatch = "2017-12-30T22:41:37 24762.175 INF Chat: 'ServerAS': Server Beltarting In 3 Minutes."

    val restartParser = ServerRestartingParser()

    @Test
    fun couldMatch() {
        assertTrue(restartParser.couldMatch(shouldMatch))
        assertFalse(restartParser.couldMatch(shouldNotMatch))
    }

    @Test(expected = FatalParseError::class)
    fun parseNotMatch() {
        restartParser.parse(shouldNotMatch)
    }

    @Test
    fun parseMatch() {
        val parsed = restartParser.parse(shouldMatch)
        assertEquals(3, parsed.restartInMinutes)
    }
}
package net.zefiris.sdtd.servermanager.server.telnet.parser.executeCommandParser

import org.junit.Test

import org.junit.Assert.*

class ExecuteCommandParserTest {
    val doesMatch= "2018-01-01T15:30:09 4713.706 INF Executing command 'bc-lp' by Telnet from 37.201.5.61:56254"
    val doesNotMatch = "2017-12-03T19:57:10 3188.706 INF Time: 51.58m FPS: 34.58 Heap: 1982.1MB Max: 2694.9MB Chunks: 2019 CGO: 115 Ply: 4 Zom: 1 Ent: 29 (47) Items: 1 CO: 4 RSS: 5706.6MB"

    @Test
    fun couldMatch() {
        val parser = ExecuteCommandParser()
        assertTrue(parser.couldMatch(doesMatch))
        assertFalse(parser.couldMatch(doesNotMatch))
    }

    @Test(expected = FatalParseError::class)
    fun parseFailed() {
        ExecuteCommandParser().parse(doesNotMatch)
    }

    @Test
    fun parse() {
        val executeCommandDTO = ExecuteCommandParser().parse(doesMatch)
        assertEquals("bc-lp", executeCommandDTO.command)
        assertEquals("37.201.5.61", executeCommandDTO.ip)
        assertEquals(56254, executeCommandDTO.port)
    }

}